package oli.num {
	/**
	 * ...
	 * @author Oliver Ross
	 */
	public class RomanNumeralGenerator implements IRomanNumeralGenerator
	{
		private static const NUMERAL:uint = 0; 
		public static const DECIMAL:uint = 1;	// exposed for test
		public static const numeralObjects:Array = [	// exposed for test
			['M',	1000],
			['CM',	900],
			['D', 	500],
			['CD',	400],
			['C',	100],
			['XC',	90],
			['L',	50],
			['XL',	40],
			['X',	10],
			['IX',	9],
			['V',	5],
			['IV',	4],
			['I',	1]
		];
		
		public function RomanNumeralGenerator() { }
		
		public function generate(decimal:uint):String {
			return getNumeral(decimal);
		}
		private function getNumeral(decimal:uint, numeral:String = ''):String {
			if (decimal == 0) { return numeral; }
			var obj:Array = getNumeralObject(decimal);
			return getNumeral(decimal - obj[DECIMAL], numeral + obj[NUMERAL]);
		}
		private function getNumeralObject(decimal:uint):Array {
			for (var i:int = 0; i < numeralObjects.length; i++) {
				if (decimal >= numeralObjects[i][DECIMAL]) { return numeralObjects[i]; }
			}
			return ['',0];
		}		
	}
}