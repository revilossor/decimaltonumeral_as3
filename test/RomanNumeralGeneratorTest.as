package {
	import flexunit.framework.Assert;
	import oli.num.RomanNumeralGenerator;
	/**
	 * ...
	 * @author Oliver Ross
	 */
	public class RomanNumeralGeneratorTest
	{
		private static const INPUT:uint = 0; private static const OUTPUT:uint = 1;
		private var _generator:RomanNumeralGenerator;
		
		[Before]
		public function create():void {
			_generator = new RomanNumeralGenerator();
		}
		
		[Test]
		public function testNumeralObjectsAreInCorrectOrder():void {
			var correctResults:uint = 0
			for (var i:int = 0; i < RomanNumeralGenerator.numeralObjects.length - 1; i++) {
				if (RomanNumeralGenerator.numeralObjects[i][RomanNumeralGenerator.DECIMAL] >
					RomanNumeralGenerator.numeralObjects[i + 1][RomanNumeralGenerator.DECIMAL]) {
						correctResults++;
					}
			}
			Assert.assertEquals(RomanNumeralGenerator.numeralObjects.length - 1, correctResults); 
		}
		[Test]
		public function testCorrectReturnValues():void {
			var correctResults:uint = 0;
			for (var i:int = 0; i < Samples.inputsAndOutputs.length; i++) {
				if (_generator.generate(Samples.inputsAndOutputs[i][INPUT]) == Samples.inputsAndOutputs[i][OUTPUT]) {
					correctResults++;
				}
			}
			Assert.assertEquals(Samples.inputsAndOutputs.length, correctResults);
		}
		[After]
		public function destroy():void {
			_generator = null;
		}
		
	}
}