package app 
{
	import oli.num.RomanNumeralGenerator;
	/**
	 * ...
	 * @author Oliver Ross
	 */
	public class Numerals 
	{
		private var _testNumber:uint = 3999;
		private var _generator:RomanNumeralGenerator;
		
		public function Numerals() 
		{
			init();
			convert(_testNumber);
		}
		private function init():void {
			_generator = new RomanNumeralGenerator();
		}
		private function convert(decimal:uint):void {
			trace(decimal + ' in roman numerals is ' + _generator.generate(decimal));
		}
		
	}

}