package oli.num 
{
	
	/**
	 * ...
	 * @author Oliver Ross
	 */
	public interface IRomanNumeralGenerator 
	{
		function generate(decimal:uint):String;
	}
	
}